package pers.apricot.domain;

import io.ebean.Model;
import io.ebean.annotation.WhenCreated;
import io.ebean.annotation.WhenModified;

import javax.persistence.*;
import java.time.Instant;

@MappedSuperclass
public abstract class BaseDomain extends Model {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Version
	private Long version;

	@WhenCreated
	private Instant whenCreated;

	@WhenModified
	private Instant whenModified;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Instant getWhenCreated() {
		return whenCreated;
	}

	public void setWhenCreated(Instant whenCreated) {
		this.whenCreated = whenCreated;
	}

	public Instant getWhenModified() {
		return whenModified;
	}

	public void setWhenModified(Instant whenModified) {
		this.whenModified = whenModified;
	}
}
