package pers.apricot.domain.query.assoc;

import io.ebean.typequery.PInstant;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQAssocBean;
import io.ebean.typequery.TQProperty;
import io.ebean.typequery.TypeQueryBean;
import pers.apricot.domain.CustomerDomain;
import pers.apricot.domain.query.QCustomerDomain;

/**
 * Association query bean for AssocCustomerDomain.
 * <p>
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QAssocCustomerDomain<R> extends TQAssocBean<CustomerDomain, R> {

	public PLong<R> id;
	public PLong<R> version;
	public PInstant<R> whenCreated;
	public PInstant<R> whenModified;
	public PString<R> name;

	/**
	 * Eagerly fetch this association loading the specified properties.
	 */
	@SafeVarargs
	public final R fetch(TQProperty<QCustomerDomain>... properties) {
		return fetchProperties(properties);
	}

	public QAssocCustomerDomain(String name, R root) {
		super(name, root);
	}
}
