package pers.apricot.domain.query;

import io.ebean.EbeanServer;
import io.ebean.typequery.PInstant;
import io.ebean.typequery.PLong;
import io.ebean.typequery.PString;
import io.ebean.typequery.TQRootBean;
import io.ebean.typequery.TypeQueryBean;
import pers.apricot.domain.CustomerDomain;

/**
 * Query bean for CustomerDomain.
 * <p>
 * THIS IS A GENERATED OBJECT, DO NOT MODIFY THIS CLASS.
 */
@TypeQueryBean
public class QCustomerDomain extends TQRootBean<CustomerDomain, QCustomerDomain> {

	private static final QCustomerDomain _alias = new QCustomerDomain(true);

	/**
	 * Return the shared 'Alias' instance used to provide properties to
	 * <code>select()</code> and <code>fetch()</code>
	 */
	public static QCustomerDomain alias() {
		return _alias;
	}

	public PLong<QCustomerDomain> id;
	public PLong<QCustomerDomain> version;
	public PInstant<QCustomerDomain> whenCreated;
	public PInstant<QCustomerDomain> whenModified;
	public PString<QCustomerDomain> name;


	/**
	 * Construct with a given EbeanServer.
	 */
	public QCustomerDomain(EbeanServer server) {
		super(CustomerDomain.class, server);
	}

	/**
	 * Construct using the default EbeanServer.
	 */
	public QCustomerDomain() {
		super(CustomerDomain.class);
	}

	/**
	 * Construct for Alias.
	 */
	private QCustomerDomain(boolean dummy) {
		super(dummy);
	}
}
