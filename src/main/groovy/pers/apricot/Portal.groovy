package pers.apricot

import com.google.inject.Guice
import com.google.inject.Injector
import io.ebean.EbeanServer
import io.ebean.annotation.Transactional
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import pers.apricot.base.MineModule
import pers.apricot.domain.CustomerDomain
import pers.apricot.domain.query.QCustomerDomain

import java.sql.ResultSet
import java.sql.SQLException

class Portal {
	static final Injector INJECTOR = Guice.createInjector(new MineModule())

	static final int COUNT = 100

	static void main(String[] args) {
//		CustomerService customerService = INJECTOR.getInstance(CustomerService)
//		List<CustomerDomain> list = customerService.fuzzyFind(new CustomerDomain(name: 'abc'))
		for (int i = 0; i < COUNT; i++) {
			QCustomerDomain qCustomerDomain = new QCustomerDomain()
			List<CustomerDomain> list = qCustomerDomain.findList()
		}
		long time1 = System.nanoTime()
		for (int i = 0; i < COUNT; i++) {
			QCustomerDomain qCustomerDomain = new QCustomerDomain()
			List<CustomerDomain> list = qCustomerDomain.findList()
		}
		long time2 = System.nanoTime()
		JdbcTemplate jdbcTemplate = INJECTOR.getInstance(JdbcTemplate)
		RowMapper<CustomerDomain> rowMapper = new RowMapper<CustomerDomain>() {
			@Override
			CustomerDomain mapRow(ResultSet rs, int rowNum) throws SQLException {
				[id          : rs.getInt(1), version: rs.getLong(2), whenCreated: rs.getTimestamp(3).toInstant(),
				 whenModified: rs.getTimestamp(4).toInstant(), name: rs.getString(5)] as CustomerDomain
			}
		}
		for (int i = 0; i < COUNT; i++) {
			List<CustomerDomain> list = jdbcTemplate.query(/select * from customer/, rowMapper)
		}
		long time3 = System.nanoTime()
		for (int i = 0; i < COUNT; i++) {
			List<CustomerDomain> list = jdbcTemplate.query(/select * from customer/, rowMapper)
		}
		long time4 = System.nanoTime()
		println time2 - time1
		println time4 - time3
		EbeanServer ebeanServer = INJECTOR.getInstance(EbeanServer)
		Portal portal = new Portal()
		portal.save()
		long time5 = System.nanoTime()
		portal.save()
		long time6 = System.nanoTime()
		for (int i = 0; i < COUNT; i++) {
			jdbcTemplate.update(/insert into customer (name) values ('test')/)
//			jdbcTemplate.update(/insert into customer (name) values (?)/, 'test')
		}
		long time7 = System.nanoTime()
		for (int i = 0; i < COUNT; i++) {
			jdbcTemplate.update(/insert into customer(name) values('test') /)
//			jdbcTemplate.update(/insert into customer (name) values (?)/, 'test')
		}
		long time8 = System.nanoTime()
		println time6 - time5
		println time8 - time7
	}

	@Transactional()
	void save() {
		for (int i = 0; i < COUNT; i++) {
			//			SqlUpdate sqlUpdate = ebeanServer.createSqlUpdate(/insert into customer (name) values (?)/)
//			sqlUpdate.setParameter(1, 'test')
//			ebeanServer.execute(sqlUpdate)
			CustomerDomain customerDomain = [name: 'test'] as CustomerDomain
			customerDomain.save()
		}
	}

}
