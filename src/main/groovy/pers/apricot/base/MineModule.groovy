package pers.apricot.base

import com.google.inject.AbstractModule
import io.ebean.EbeanServer
import org.springframework.jdbc.core.JdbcTemplate

class MineModule extends AbstractModule {

	@Override
	protected void configure() {
//		bind(EbeanServer.class).toProvider(EbeanServerProvider.class).asEagerSingleton()
		bind(JdbcTemplate.class).toProvider(JdbcTemplateProvider.class).asEagerSingleton()
	}
}
