package pers.apricot.base

import com.google.inject.Provider
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ebean.EbeanServer
import io.ebean.EbeanServerFactory
import io.ebean.config.ServerConfig

class EbeanServerProvider implements Provider<EbeanServer> {

	@Override
	EbeanServer get() {
		ServerConfig config = new ServerConfig()
		config.setDefaultServer(true)
		config.setRegister(true)

		Properties properties = new Properties()
		properties.load(this.class.getResourceAsStream('/HikariCP.properties'))
		HikariConfig hikariConfig = new HikariConfig(properties)
		HikariDataSource dataSource = new HikariDataSource(hikariConfig)

		config.setDataSource(dataSource)

		return EbeanServerFactory.create(config)
	}
}
