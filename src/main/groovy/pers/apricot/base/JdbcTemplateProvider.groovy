package pers.apricot.base

import com.google.inject.Provider
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.springframework.jdbc.core.JdbcTemplate

class JdbcTemplateProvider implements Provider<JdbcTemplate> {
	@Override
	JdbcTemplate get() {
		Properties properties = new Properties()
		properties.load(this.class.getResourceAsStream('/HikariCP.properties'))
		HikariConfig hikariConfig = new HikariConfig(properties)
		HikariDataSource dataSource = new HikariDataSource(hikariConfig)
		return new JdbcTemplate(dataSource)
	}
}
