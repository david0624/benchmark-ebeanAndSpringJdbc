package pers.apricot.service

import com.google.inject.Singleton
import io.ebean.Ebean
import io.ebean.ExpressionList
import pers.apricot.domain.CustomerDomain

@Singleton
class CustomerService {

	List<CustomerDomain> fuzzyFind(CustomerDomain customerDomain) {
		ExpressionList<CustomerDomain> expressionList = Ebean.find(CustomerDomain).where()
		Map<String, Object> properties = customerDomain.getProperties()
		properties.remove('class')
		properties.each { String key, Object value ->
			if (value) {
				expressionList.eq(key, value)
			}
		}
		expressionList.findList()
	}
}
