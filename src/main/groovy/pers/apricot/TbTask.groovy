package pers.apricot


import java.time.LocalDateTime

class TbTask {

    Long id

    String projectName
    String groupName
    String stageName
    Long stageId

    Integer taskOrder // 排序

    String title // 标题
    String assignee // 分配给

    /**
     * 任务号,全局唯一
     */
    String globalNo  // auto gen
    /**
     * 任务组别
     */
    String taskGroup  // text
    /**
     * 任务类型
     */
    String taskType  // select
    /**
     * 需求消息描述
     */
    String requireDetail  // textarea
    /**
     * 方案详细设计
     */
    String designDetail // textarea
    /**
     * 技术设计描述
     */
    String implementDetail // textarea
    /**
     * 测试用例
     */
    String testCase // textarea
    /**
     * 参与者
     */
    String participants // multi select
    /**
     * 开发截止时间
     */
    LocalDateTime deadline // datetime
    /**
     * 计划验收时间
     */
    LocalDateTime examineDatetime // datetime
    /**
     * 优先级
     */
    String priority // select
    /**
     * 工时
     */
    Integer spendHours // int
    /**
     * 进度
     */
    Integer percentage // int
    /**
     * 标签
     */
    String labels // labels
    /**
     * 发布说明
     */
    String releaseNote // textarea
    /**
     * 代码审核
     */
    String codeReview // textarea
    /**
     * 测试者
     */
    String testers // multi select
    /**
     * 修复至其它项目
     */
    String repairedProjects // multi select

    /**
     * 项目关闭
     */
    int closed = 0 // 任务状态
    /**
     * 完成时间
     */
    LocalDateTime closedDatetime // 完成时间
    LocalDateTime created
    String createdBy
    LocalDateTime lastUpdated
    String lastUpdatedBy
    int version
}
