package pers.apricot

import com.google.inject.Guice
import com.google.inject.Injector
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import pers.apricot.base.MineModule

class PortalJdbc {
    static final Injector INJECTOR = Guice.createInjector(new MineModule())

    static final int COUNT = 100

    static void main(String[] argTbTasks) {
        JdbcTemplate jdbcTemplate = INJECTOR.getInstance(JdbcTemplate)
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName("tb_task")
        Map insert = ["stageId"        : 6, "title": "123", "taskType": "产品基本功能", "priority": "3", "requireDetail": "123",
                      "designDetail"   : "123", "implementDetail": "123", "testCase": "13221", "assignee": "admin",
                      "participants"   : "admin,250-ADMIN", "deadline": "2020-05-29 00:00:00",
                      "examineDatetime": "2020-05-07 00:00:00", "spendHours": "5", "labels": "A,B",
                      "testers"        : "350-ADMIN", "percentage": 80, "releaseNote": "13213",
                      "taskOrder"      : 999]
        for (int i = 0; i < 5; i++) {
            simpleJdbcInsert.execute(insert)
        }
    }
}