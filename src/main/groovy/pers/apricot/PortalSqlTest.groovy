package pers.apricot

import com.google.inject.Guice
import com.google.inject.Injector
import groovy.transform.TypeChecked
import pers.apricot.base.MineModule
import pers.apricot.domain.CustomerDomain
import pers.apricot.domain.query.QCustomerDomain

@TypeChecked
class PortalSqlTest {

	static final Injector INJECTOR = Guice.createInjector(new MineModule())

	static void main(String[] args) {
		QCustomerDomain qCustomerDomain = new QCustomerDomain()
		qCustomerDomain.id.gt(0)
		List<CustomerDomain> list = qCustomerDomain.findList()
		println(list)
		println(qCustomerDomain.generatedSql)
	}
}
